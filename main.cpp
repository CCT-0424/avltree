#include <iostream>

using namespace std;

//AVL node
struct node {
    int numero;
    node *left;
    node *right;
};

//Inicializa o nó raiz da AVL Tree (este nó pode mudar de valor)
node *root = nullptr;

//Calcula a altura da árvore
int altura(node *temp){
    int alt = 0;
    if (temp != nullptr){
        int alt_esq = altura(temp->left);
        int alt_dir = altura(temp->right);
        int alt_max = max(alt_esq,alt_dir);
        alt = alt_max + 1;
    }
    return alt;
}

//Calcula o fator de balanceamento
int fator_balanceamento(node *temp){
    int alt_esq = altura(temp->left);
    int alt_dir = altura(temp->right);
    int fator_b = alt_esq - alt_dir;
    return fator_b;
}

//Fator de balanceamento decide o tipo de rotação deverá ser feita

//Rotação direita -> direita
node *rotacao_dd(node *pai){
    node *temp;
    temp = pai->right;
    pai->right = temp->left;
    temp->left = pai;
    return temp;
}

//Rotacao esquerda -> esquerda

node *rotacao_ee(node *pai){
    node *temp;
    temp = pai->left;
    pai->left = temp->right;
    temp->right = pai;
    return temp;
}

//Rotacao esquerda -> direita
node *rotacao_ed(node *pai){
    node *temp;
    temp = pai->left;
    pai->left = rotacao_dd(temp);
    return rotacao_ee(pai);
}

//Rotacao direita -> esquerda
node *rotacao_de(node *pai){
    node *temp;
    temp = pai->right;
    pai->right = rotacao_ee(temp);
    return rotacao_dd(pai);
}

//Balancear arvore
node *balancear(node *temp){
    int fator = fator_balanceamento(temp);
    if (fator > 1){
        if (fator_balanceamento(temp->left) > 0)
            temp = rotacao_ee(temp);
        else
            temp = rotacao_ed(temp);
    } else
    if (fator < -1){
        if (fator_balanceamento(temp->right) > 0)
            temp = rotacao_de(temp);
        else
            temp = rotacao_dd(temp);
    }
    return temp;
}

//Inserir na árvore
node *inserir(node *root, int valor){
    if (root == nullptr){
        root = new node;
        root->numero = valor;
        root->left = nullptr;
        root->right = nullptr;
        return root;
    } else
    if (valor < root->numero){
        root->left = inserir(root->left,valor);
        root = balancear(root);
    } else
    if (valor >= root->numero){
        root->right = inserir(root->right,valor);
        root = balancear(root);
    }
    return root;
}


int main()
{
    inserir(root,2);
    inserir(root,3);
    inserir(root,1);
    inserir(root,4);
    cout << "AVL Tree" << endl;
    return 0;
}
